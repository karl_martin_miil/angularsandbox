var app = angular.module("sandboxApp", ["ngRoute"])
    .config(function ($locationProvider, $routeProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $routeProvider.when("/", {
            templateUrl: "views/main.html",
            controller: "MainController"
        }).when("/dogs", {
            templateUrl: "views/list.html",
            controller: "list"
        }).when("/detail/:id", {
            templateUrl: "../views/detail.html",
            controller: "detail"
        }).when("/add", {
            templateUrl: "views/add.html",
            controller: "add"
        });
    })
    .run(function ($rootScope) {
        $rootScope.list = [];
    })
    .controller('MainController', function($scope, $route, $location) {
        $scope.$route = $route;
        $scope.$location = $location;
    })
    .controller("list", function ($scope, $rootScope) {
        $scope.list = $rootScope.list;
    }).controller("detail", function ($scope, $route, $rootScope) {
        $scope.dog = $rootScope.list[$route.current.params.id];
    }).controller("add", function ($scope, $rootScope) {
        $scope.add = function () {
            var id = $rootScope.list.length;
            $scope.new.id = id;
            $rootScope.list.push($scope.new);
            $scope.new = {};
            $scope.form.$setUntouched();
        }
    });
